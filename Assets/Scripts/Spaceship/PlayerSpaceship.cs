using System;
using Manager;
using UnityEngine;
using UnityEngine.UI;

//spaceshipPlayer
namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private Bullet bullet2;
        private Bullet myBullet;


        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        
        
        //sound
        public void Fire1()
        {
            myBullet = defaultBullet;
            Fire();
            AudioManager.Instance.Play("Laser2");
        }
        public void Fire2()
        {
            myBullet = bullet2;
            Fire();
            AudioManager.Instance.Play("Laser1");
        }

        public override void Fire()
        {
            var bullet = Instantiate(myBullet, gunPosition.position, Quaternion.identity);
            bullet.Init();
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            AudioManager.Instance.Play("Explosion2");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}