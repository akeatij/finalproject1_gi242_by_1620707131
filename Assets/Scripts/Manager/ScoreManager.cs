﻿using TMPro;
using UnityEngine;
using System;
using Utilities;
using System;

namespace Manager
{
    public class ScoreManager : Singleton<ScoreManager>
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        private GameManager gameManager;

        public void Init(GameManager gmanager)
        {
            gameManager = gmanager;
            gameManager.OnRestarted += OnRestarted;
            
            HideScore(false);
            SetScore(0);
        }

        public void SetScore(int score)
        {
            scoreText.text = $"Score : {score}";
        }
        
        private void OnRestarted()
        {
            gameManager.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}


